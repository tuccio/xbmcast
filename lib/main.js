var data = require("sdk/self").data;

var contents = [
	require("./youtube"),
	require("./magnet"),
	require("./twitch"),
	require("./videofile")
];

var caster = require("./caster");
var json = null;

/* Context menu */

var cm = require("sdk/context-menu");
var castContextOptionLabel = "Cast on XBMC";

var castContextOption = cm.Item({

	label: castContextOptionLabel,
	
	context: cm.PredicateContext(contentPredicate),
	
	contentScript: 'self.on("context", function () {' +
				   '  return true;' +
				   '});' +
				   'self.on("click", function() {' +
				   '  self.postMessage("click");' +
				   '});',
				   
	onMessage: messageHandler			   

});

/* Content predicate
 * Returns true if the context is a known content
 */

function contentPredicate(context) {

	var found = false;	
	json = null;

	contents.forEach(function (content) {
		
		if (!found) {
		
			var result = content.testContext(context);
			
			if (result.content) {
			
				json = result.json;				
				found = true;
				
			}
			
		}
		
	});
	
	return found;

}

/* Message handler gets called when the context menu item is clicked */

function messageHandler(msg) {
	
	if (json) {
	
		caster.castJSON(json);
	
	}

}