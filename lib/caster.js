function castJSON(json) {

	const { Cc, Ci } = require("chrome");

	var sp      = require("sdk/simple-prefs").prefs;
	var request = Cc["@mozilla.org/xmlextras/xmlhttprequest;1"].createInstance(Ci.nsIXMLHttpRequest);
	
	var auth;
	
	if (sp.username && sp.password) {
		auth = sp.username + ":" + sp.password + "@";	
	}
	else {
		auth = "";
	}
	
	var tAddress = sp.address + ":" + sp.port;
	var url      = "http://" + auth + tAddress + "/jsonrpc";
	             
	var request  = new XMLHttpRequest();
	
	if (sp.notifications) {
	
		var notifications = require("sdk/notifications");
	
		request.onload = function () {
			
			var response = JSON.parse(this.responseText);
			
			var result = null;
			
			if (response.result == "OK") {
				result = "Cast to " + tAddress + " succeeded.";
			}
			else {
				result = "Cast failed (" + result + ").";
			}
			
			notifications.notify({
				title: "xbmcast",
				text: result
			});
			
		};
	
	}
	
	request.open("POST", url);			
	request.setRequestHeader("Content-Type", "application/json");
	
	request.send(json);

}

exports.castJSON = castJSON;