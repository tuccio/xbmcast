var regex = /([a-z]+):\/\/.*\.(mkv|avi|ogg|ogv|flv|wmv|mpg|mp2|mpeg|mp4|m4p|mpv|3gp|mov|qt)/i

function videofileTest(context) {
	
	var content = false;
	var json = null;
	
	var url = context.linkURL;
	var match = regex.exec(url);
	
	if (match) {
	
		content = true;
		
		json = JSON.stringify({
			jsonrpc : "2.0",
			method : "Player.Open",
			params : {
				item : {
					file : match[0]
				}
			},
			id : 1
		});
		
	}
	
	return {
		content : content,
		json : json
	};

}

exports.label       = "videofile";
exports.testContext = videofileTest;