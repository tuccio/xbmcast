var regex = /^http(?:s)?:\/\/.*.twitch.tv\/(.+)/;

function twitchTest(context) {
	
	var content = false;
	var json = null;
	
	var url = (context.linkURL != null ? context.linkURL : context.documentURL);
	var match = regex.exec(url);
	
	if (match) {
	
		content = true;
		
		json = JSON.stringify({
			jsonrpc : "2.0",
			method : "Player.Open",
			params : {
				item : {
					file : "plugin://plugin.video.twitch/playLive/" + match[1] + "/"
				}
			},
			id : 1
		});
		
	}
	
	return {
		content : content,
		json : json
	};

}

exports.label       = "twitch";
exports.testContext = twitchTest;