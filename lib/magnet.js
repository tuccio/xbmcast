
var regex = /^magnet:\?xt=urn:btih:.*/;

function magnetTest(context) {
	
	var content = false;
	var json = null;
	
	var url = context.linkURL;
	
	if (url && regex.test(url)) {
	
		content = true;
			
		json = JSON.stringify({
			jsonrpc : "2.0",
			method : "Player.Open",
			params : {
				item : {
					file : "plugin://plugin.video.xbmctorrent/play/" + encodeURIComponent(url)
				}
			},
			id : 1
		});
	
	}
	
	return {
		content : content,
		json : json
	};

}

exports.label       = "magnet";
exports.testContext = magnetTest;