var regex = /https?:\/\/(?:www\.)?youtu(?:\.be\/|be.com\/.*watch?.*v=)([A-Za-z0-9\-_]+).*/

function youtubeTest(context) {
	
	var content = false;
	var json = null;
	
	var url = (context.linkURL != null ? context.linkURL : context.documentURL);
	var match = regex.exec(url);
	
	if (match) {
	
		content = true;
		
		json = JSON.stringify({
			jsonrpc : "2.0",
			method : "Player.Open",
			params : {
				item : {
					file : "plugin://plugin.video.youtube/?action=play_video&videoid=" + match[1]
				}
			},
			id : 1
		});
		
	}
	
	return {
		content : content,
		json : json
	};

}

exports.label       = "youtube";
exports.testContext = youtubeTest;